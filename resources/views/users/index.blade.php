@extends('layouts.app')

@section('content')
    <ul>
    @foreach($users as $user)
        <li>{{$user->name}} => {{$user->gender->gender}}</li>
    @endforeach
    </ul>
@endsection