@extends('layouts.app')

@section('content')

<h2>Gender Stat</h2>
<table class="table">
    <thead>
        <tr>
            <td>Name</td>
            <td>Percentage</td>
        </tr>
    </thead>
    <tbody>
        @foreach($statGender as $stat)
        <tr>
            <td>{{$stat['gender']}}</td>
            <td>{{$stat['percentage']}}</td>
        </tr>
        @endforeach
    </tbody>
</table>

<h2>Admin Stat</h2>
<table class="table">
    <thead>
        <tr>
            <td>Admin</td>
            <td>Not Admin</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{$statsAdmin}}</td>
            <td>{{$statsOther}}</td>
        </tr>
    </tbody>
</table>

<h2>Names Stat</h2>
<table class="table">
    <thead>
        <tr>
            <td>Admin</td>
            <td>Not Admin</td>
        </tr>
    </thead>
    <tbody>
    @foreach($statsName as $stat)
        <tr>
            <td>{{$stat->name}}</td>
            <td>{{$stat->name_occurences}}</td>
        </tr>
    @endforeach
    </tbody>
</table>

@endsection