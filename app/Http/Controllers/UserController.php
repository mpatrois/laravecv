<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Gender;
use DB;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('users.index',[
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function admin()
    {
        $statGender = [];
        $totalUsers = User::count();
        foreach (Gender::all() as $gender) {
            $statGender[] = [
                'gender'     => $gender->gender,
                'percentage' => User::where('gender_id',$gender->id)->count() * 100 / $totalUsers,
            ];
        };

        $statAdmin = [
            'isAdmin' => User::where('is_admin',true)->count() * 100 / $totalUsers,
            'isAdmin' => User::where('is_admin',true)->count() * 100 / $totalUsers,
        ];

        $statAdmin = User::where('is_admin',true)->count() * 100 / $totalUsers;

        $statsName = DB::table('users')
                     ->selectRaw('name, COUNT(name) AS name_occurences')
                     ->groupBy('name')
                     ->orderBy('name_occurences','desc')
                     ->get();

        return view('admin.index',[
            'statGender' => $statGender,
            'statsAdmin' => $statAdmin,
            'statsOther' => 100 - $statAdmin,
            'statsName'  => $statsName
        ]);
    }
}
