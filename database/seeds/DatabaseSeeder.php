<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Gender;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        Gender::create([
            'id'   => 1,
            'gender' => 'female',
        ]);
        
        Gender::create([
            'id'   => 2,
            'gender' => 'male',
        ]);
        
        Gender::create([
            'id'   => 3,
            'gender' => 'other',
        ]);

         User::create([
            'name'      => 'Manu',
            'email'     => 'manu@manu.fr',
            'password'  => bcrypt('motdepasse'),
            'gender_id' => rand(1,2),
            'is_admin'  => true,
        ]);
         
        User::create([
            'name'      => 'Manu',
            'email'     => 'manu2@manu.fr',
            'password'  => bcrypt('motdepasse'),
            'gender_id' => rand(1,2),
            'is_admin'  => false,
        ]);

        for ($i=0; $i < 10 ; $i++) { 
            User::create([
                'name'      => $faker->name,
                'email'     => $faker->unique()->safeEmail,
                'password'  => bcrypt('motdepasse'),
                'gender_id' => rand(1,3),
            ]);
        }

    }
}
